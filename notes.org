* presentation
- adjust browser window width until slide height is correct.
- the slides in ./presented were editing after the talk to fix some
  typos.
** pl club announcement

== Friday 14 February: Generic Haskell Call-tracing Decorator ==

'''Location''': FAB 86-01, 12:30 pm

'''Speaker''': Nathan Collins (Portland State)

'''Abstract''': The Python programming language supports a
generic-programming idiom called "decoration", which allows the
programmer to easily add functionality to an existing function without
editing it internally. Common decorators include generic memoization
and call-tracing.  In this talk I will show how to simulate Python's
decorator idiom in Haskell, and describe a very general purpose
Haskell call-tracing framework I've built using the idiom.  Most of
the work here comes down to figuring out how to do intuitively simple
generic programming in a type-safe way, but along the way we'll see
some cool tricks, including a first-class notion of implication
between Haskell constraints.

The complete code for the library is here:
https://github.com/ntc2/haskell-call-trace

'''Homework''':

''Easy:''

Read this Stack Overflow answer where I describe how to produce LaTeX
natural-deduction proofs by call-tracing an STLC type checker:
http://stackoverflow.com/questions/19919431/latex-natural-deduction-proofs-using-haskell/20829134#20829134

''Harder:''

Define a notion of Haskell unary-constraint implication which is
strong enough to support casting.

For example, suppose you define unary-constraint conjunction by
{{{
  class    (c1 a , c2 a) => (c1 :&&: c2) a
  instance (c1 a , c2 a) => (c1 :&&: c2) a
}}}
Your goal then is to define a type
{{{
  Implies :: (* -> Constraint) -> (* -> Constraint) -> *
}}}
and a function
{{{
  cast :: Implies c1 c2 -> H c1 -> H c2
}}}
where
{{{
  data H (c :: * -> Constraint) where
    MkH :: c a => a -> H c
}}}
s.t. the types
{{{
  Implies c1           c1 -- Always
  Implies (c1 :&&: c2) c1 -- Always
  Implies (c1 :&&: c2) c2 -- Always
  Implies c1           c2 -- When 'c1' is declared as 'class (c2 a ,
...) => c1 a where ...'
}}}
are inhabited (non-trivially) for all unary constraints `c1` and `c2`
(you only need to exhibit witnesses for particular choices of `c1` and
`c2`, but your witnesses should not depend on the values of `c1` and
`c2` you chose).

* slide TODOs
- [ ] make slides float to top of page if necessary to make room
- [ ] add cha-ching sound in some transition
* problem summary: *low cost* *high flexibility* logging
* solution summary:
- open recursion with a generic logging "shim" tied into the knot:
  - open-f r = ... r y ...
  - shim h = <do before stuff> call h <do after stuff>
  - f = (shim open-f) f
- generic logging shim produces a stream of log events
  - "begin" events on entry, which log arguments and requested initial
    state
  - "end" events on return, which log result and any requested final
    state
- log event stream is parsed into a log tree, with inferred blame for
  errors
  - think of "begin" as a tagged open paren and "end" as a tagged
    closed paren. parsing takes the paren sequence to a balanced paren
    tree.
  - if the close parens don't match the open parens, then there is an
    error. we infer that the error source was during the call
    corresponding to the last open paren.
  - it's not obvious to me now that all this is necessary for error
    handling, but it seemed like the most obvious approach at the
    time.
    - ignoring errors, a simpler log shim is something like

        -- Ignoring the before and after state
        log f xs = do
          priorCalls <- getForest
          setForest empty
          r <- f xs
          subs <- getForest
          setForest $ priorCalls `append` <call 'f' 'xs' return 'r' with sub calls 'subs'>
  
      however, that seems to require being in some kind of state monad,
      for the get/set forest calls.
  
      can probably get away with only a writer monad with something like
      this:
  
        log f xs = do
          censorWithResult p (f xs)
          where
            p r subs = [<call 'f' 'xs' return 'r' with sub calls 'subs'>]
  
      where
  
        censorWithResult :: (a -> w -> w) -> Writer w a -> Writer w a
  
      is a better version of 'Control.Monad.Censor' which I don't see
      how to write using 'tell', 'listen', and 'pass', the 'Writer'
      primitives.
    - then, to add errors, we "just" wrap the monadic calls in error
      handlers that capture the results? the point is that we probably
      need some kind of error catching to recover the stream in the
      current implementation (e.g., if we are storing the stream in
      some mutable state variable then we can catch even 'error',
      since we're in 'IO', and if we're in a regular error monad, then
      we can use it's catch function).
- the logging shim is defined using some tricks
  - we automatically compute the arguments and result of a monadic
    function, by identifying the monadic base case
    - it seems you can't write the type function with a non-monadic
      base case in GHC 7.6.3, but closed type families in 7.8 should
      support this.
    - on the other hand, it's actually ambiguous: we may really want
      to think of a function of type 'a -> b -> c' not as an uncurried
      '(a , b) -> c' but as a higher order 'a -> (b -> c)'. so, in the
      non-monadic case (where we would use unsafePerformIO to log to a
      mutable state variable).

      so, e.g., we might have a unsafeShim which takes a type-level
      int argument which says how many arguments to gobble up.
  - to actually call the monadic function, we build up a continuation.
    - may not be absolutely necessary, but a continuation is the first
      thing i found that worked.
    - i think the problem was that we need the inner most (earliest
      logged) argument first, so the nested tuple built up by the shim
      is backwards. now it's not obvious why we couldn't just reverse
      the tuple, but looking at the code and/or my notes should help
      ...
- the logstream and logtree types are interesting heterogeneous types
  - they must be heterogeneous because we want to save the arguments
    and results for arbitrary post processing
  - because they're heterogenous, we need to "bound" the type of their
    contents so that we can post process them later.
  - the way we write post processor classes (which seems to be the obvious
    way), they require that they be given a log tree whose elements
    are constrained by the post processor class being defined.

    e.g.

      class Process where
        callAndReturn :: LogTree Process "callAndReturn" -> String
        callAndError  :: LogTree Process "callAndError" -> String

      process :: LogTree Process -> String
      process = apply 'callAndReturn' or 'callAndError' as necessary
                map ourself over the children
                combine the results

  - but then if we want to post process two different ways, we have a
    problem: if

      t :: LogTree (P1 :&&: P2)

    then we need to get the casts

      t1 :: LogTree P1
      t2 :: LogTree P2

    if we ha defined

      class    (P1 a , P2 a) => P1P2 a
      instance (P1 a , P2 a) => P1P2 a

    then everything will just work, but then we have to define a class
    for every set of classes we want to consider. seems very clunky
    ... so we have a fancy solution.

    the fancy solution is to define constraint implication: we need
    that e.g.

      P1 :&&: P2 "implies" P1

    so that we can write a coerce function:

      coerce :: C1 "implies" C2 -> LogTree C1 -> LogTree C2

  - to distinguish nodes for different functions with the same type,
    we need to "tag" the trees with some unique type-level information
    correspond to the function. we use GHC type level strings for
    this. as a bonus, we can print the strings!

- all this gets us to a place where we can write processor instances,
  but we must write a separate instance for *every* function. this is
  useful in some cases, where we want very fine grained control
  (e.g. for a latex proof tree), but not useful in others, e.g. where
  we just want to 'Show' all the arguments and results.

  to handle the "uniform" case in an easy way, we write a transformer
  that takes a log tree and wraps all the data in bounded het
  constructors in type 'H c'. the tree is already het, but now we've
  marked the individual data, so that all tree nodes have the same
  type.  now we can write an instance for that uniform type, which
  uses the uniform bound 'c' in the het constructors.

- current processors include unix tree style and latex proof
  tree. plan to also add ascii proof tree and graphviz.

- future work includes extending these ideas to memoization (mostly
  straightforward) and hash consing (trickier).
* motivating examples

there are many technical ideas and tricks, but motivating examples
should really help people appreciate the problem and the solution.

- the mostly prepared example i have is the STLC proof trees example.

  possible versions
  - plain type checker without logging (the SO fancy traced version w/o the fancy tracing)
  - type checker with inlined latex production (the SO inlined example)
  - intermediate version that abstracts the logging into a custom latex shim
  - intermediate version that abstracts logging in generic shim,
    essentially the inlining of my generic shim, with explicit begincall
    and endcall events

- the original example is spire, which ironically i've still not
  plugged the logger into. however, i can show the hand calculated
  trace.

- an STLC + unification example would be really cool, but i probably
  won't have that by tmw. however, i have a fairly detailed
  description of what to do here in my notes.  BRING MY NOTES!

* homework ideas
- implement constraint implication
- write a type function that computes the arguments and results of a
  function type
